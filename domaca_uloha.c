#include <stdio.h>

short int swap_endianness(short int x)
{
	char p;
	char *y=&x;
	
	p=y[0];
	y[0]=y[1];
	y[1]=p;
	
	return *(short int *)y;
}

int main()
{
	short int x=123;
	
	printf("%d\n",swap_endianness(x));
	
	return 0;
}
